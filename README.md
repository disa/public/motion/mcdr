# Welcome to MCDR project

...

## Contact Person
Jan Sedmidubsky, [homepage](https://www.muni.cz/en/people/60474-jan-sedmidubsky)

## Licence
The owner of the result is Masaryk University, a public high school, ID: 00216224. Masaryk University allows other companies and individuals to use this software free of charge and without territorial restrictions under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. M-Index library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with M-Index library. If not, see [http://www.gnu.org/licenses/].