package mcdr.ui.layout.components.impl;

import java.io.IOException;
import java.util.Map;
import javax.servlet.jsp.JspWriter;
import mcdr.objects.ObjectMocapPose;
import mcdr.sequence.SequenceMocap;

/**
 *
 * @author Jan Sedmidubsky, xsedmid@fi.muni.cz, FI MU Brno, Czech Republic
 */
public class SequenceMocapPageComponentK3DPersonIdentification extends SequenceMocapPageComponentK3DSearch {

    //************ Constructors ************//
    /**
     * Creates a new instance of
     * {@link SequenceMocapPageComponentPersonIdentification}.
     *
     * @param canvasWidth canvas width
     * @param canvasHeight canvas height
     * @param numberOfSkippedFramesToDisplay number of frames that are ignored
     * between two displayed consecutive frames (the zero value means that no
     * frame is ignored)
     * @param fps data sampling frequency (frames per second)
     * @param datasets specifications of used datasets
     * @param sequenceToDatasetMap map associating a sequence with its dataset
     */
    public SequenceMocapPageComponentK3DPersonIdentification(int canvasWidth, int canvasHeight, int fps, int numberOfSkippedFramesToDisplay, Map<String, DatasetDefinition> datasets, Map<String, String> sequenceToDatasetMap) {
        super(canvasWidth, canvasHeight, fps, numberOfSkippedFramesToDisplay, datasets, sequenceToDatasetMap);
    }

    /**
     * Creates a new instance of
     * {@link SequenceMocapPageComponentPersonIdentification}.
     *
     * @param canvasWidth canvas width
     * @param canvasHeight canvas height
     * @param numberOfSkippedFramesToDisplay number of frames that are ignored
     * between two displayed consecutive frames (the zero value means that no
     * frame is ignored)
     * @param fps data sampling frequency (frames per second)
     * @param datasets specifications of used datasets
     * @param sequenceToDatasetMap map associating a sequence with its dataset
     * @param sequenceToSubjectIdMap map associating sequence id with the
     * corresponding subject id
     */
    public SequenceMocapPageComponentK3DPersonIdentification(int canvasWidth, int canvasHeight, int fps, int numberOfSkippedFramesToDisplay, Map<String, DatasetDefinition> datasets, Map<String, String> sequenceToDatasetMap, Map<String, String> sequenceToSubjectIdMap) {
        super(canvasWidth, canvasHeight, fps, numberOfSkippedFramesToDisplay, datasets, sequenceToDatasetMap, sequenceToSubjectIdMap);
    }

    //************ Methods ************//
    @Override
    protected void generateActionCanvas(JspWriter out, SequenceMocap<ObjectMocapPose> o, String sequenceId, String objId, String locator, boolean enableRetrieval, DatasetDefinition sequenceDataset) throws IOException {

        // Searching
        if (enableRetrieval) {
            out.write("<div class=\"resultAction\">\n");

            // Query identification
            out.write("    <fieldset><legend>Identification of person</legend>\n");
            out.write("        <a class=\"button button-identify-person\" id=\"identifyPerson" + objId + "\" href=\"identification?sequenceLocator=" + sequenceId + "\" onclick=\"javascript:appendLinkParams(this);\">Identify person</a>\n");
            out.write("    </fieldset>\n");

            // Query selection
            out.write("    <fieldset><legend>Retrieval of similar movements</legend>\n");
            out.write("        <div class=\"button play-selection\" id=\"playSelection" + objId + "\">\n");
            out.write("            <span class=\"btn-row\">Play the selection</span>\n");
            out.write("            <div class=\"btn-row\">\n");
            out.write("                <span class=\"range\" id=\"rangeFrom" + objId + "\">0</span> - <span class=\"range\" id=\"rangeTo" + objId + "\">100</span> frames\n");
            out.write("            </div>\n");
            out.write("        </div>\n");
            out.write("        <a class=\"button button-search\" id=\"searchLink" + objId + "\" href=\"#\" onclick=\"javascript:setSubsequenceLink('searchLink" + objId + "', " + locator + ", " + Math.max(0, o.getOffset()) + ", 'rangeFrom" + objId + "', 'rangeTo" + objId + "'); appendLinkParams(this);\">Retrieve sub-motions similar to the selection</a>\n");
            out.write("    </fieldset>");

            out.write("</div>\n");
        }

        // Subject name
        out.write("<div class=\"personName\">Person: " + (sequenceToSubjectIdMap == null ? "&lt;unknown&gt;" : sequenceToSubjectIdMap.get(sequenceId)) + "</div>\n");

        // Sequence name
        String aTagParams = "<a href=\"sequence?sequenceLocator=" + sequenceId + "\" onclick=\"javascript:appendLinkParams(this);\" >" + sequenceId + "</a>";
        if (o.getOffset() == -1 && sequenceId.equals(locator)) {
            out.write("<div class=\"resultMotionLink\">Motion " + aTagParams);
        } else {
            String subMotionPosition;
            if (o.getOffset() == -1) {
                subMotionPosition = "Cropped";
            } else {
                subMotionPosition = o.getOffset() + "-" + (o.getOffset() + o.getSequenceLength()) + " frames cropped from";
            }
            out.write("<div class=\"resultMotionLink\">" + subMotionPosition + " motion " + aTagParams);
        }
        out.write(" (" + sequenceDataset.name + ")</div>\n");
    }
}
