package mcdr.ui.layout.components.impl;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import mcdr.objects.ObjectMocapPose;
import mcdr.sequence.SequenceMocap;
import messif.objects.util.RankedAbstractObject;
import messif.ui.layout.components.PageComponent;

/**
 *
 * @author Jan Sedmidubsky, xsedmid@fi.muni.cz, FI MU Brno, Czech Republic
 */
public abstract class SequenceMocapPageComponent implements PageComponent {

    // canvas width
    protected final int canvasWidth;
    // canvas height
    protected final int canvasHeight;
    // colors of sequence skeletons
    protected final Map<String, Color> sequenceColorMap = new HashMap<>();
    // data sampling frequency (frames per second)
    protected final int fps;
    // number of frames that are ignored between two displayed consecutive frames (the zero value means that no frame is ignored)
    protected final int numberOfSkippedFramesToDisplay;
    // datasets of sequences
    protected final Map<String, DatasetDefinition> sequenceToDatasetMap;

    //************ Constructors ************//
    /**
     * Creates a new instance of {@link SequenceMocapPageComponent}.
     *
     * @param canvasWidth canvas width
     * @param canvasHeight canvas height
     * @param fps data sampling frequency (frames per second)
     * @param numberOfSkippedFramesToDisplay number of frames that are ignored
     * between two displayed consecutive frames (the zero value means that no
     * frame is ignored)
     * @param datasets specifications of used datasets
     * @param sequenceToDatasetMap map associating a sequence with its dataset
     */
    public SequenceMocapPageComponent(int canvasWidth, int canvasHeight, int fps, int numberOfSkippedFramesToDisplay, Map<String, DatasetDefinition> datasets, Map<String, String> sequenceToDatasetMap) {
        this.canvasWidth = canvasWidth;
        this.canvasHeight = canvasHeight;
        this.fps = fps;
        this.numberOfSkippedFramesToDisplay = numberOfSkippedFramesToDisplay;

        // Sequence-dataset mapping
        this.sequenceToDatasetMap = new HashMap<>();
        for (Map.Entry<String, String> entry : sequenceToDatasetMap.entrySet()) {
            this.sequenceToDatasetMap.put(entry.getKey(), datasets.get(entry.getValue()));
        }
        this.sequenceToDatasetMap.put(null, datasets.get(null));
    }

    //************ Methods ************//
    /**
     * Transforms the list of maps to a single map based on the named key-value
     * attributes.
     *
     * @param dbList list of maps (retrieved from a database)
     * @param keyAttributeName name of the column attribute used as a key
     * @param valueAttributeName name of the column attribute used as a value
     * @param includeNullValues indicates whether associations with null values
     * are stored, or not
     * @return transformed map0
     */
    public static Map<String, String> transformDBListToMap(List<Map<String, String>> dbList, String keyAttributeName, String valueAttributeName, boolean includeNullValues) {
        Map<String, String> rtv = new HashMap<>();
        for (Map<String, String> dbListEntry : dbList) {
            String value = dbListEntry.get(valueAttributeName);
            if (includeNullValues || value != null) {
                rtv.put(dbListEntry.get(keyAttributeName), value);
            }
        }
        return rtv;
    }

    /**
     * Reads the CSV-like text file and adds the key/value map entry to the
     * result map. The entry is considered in each row as the first and second
     * column.
     *
     * @param keyValueFile input CSV file
     * @return map with key-value entries read from the input file
     * @throws IOException
     */
    public static Map<String, String> transformCSVFileToMap(File keyValueFile) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(keyValueFile));
        Map<String, String> rtv = new HashMap<>();
        String line = br.readLine();
        while (line != null) {
            String[] lineSplit = line.split(";");
            rtv.put(lineSplit[0], lineSplit[1]);
            line = br.readLine();
        }
        br.close();
        return rtv;
    }

    /**
     * Reads the CSV-like text file and adds an item for each line of the file.
     * The new item is a map associating the keys (specified within the
     * mapKeyNames parameter) with the values that are gradually extracted from
     * each line in the CSV-like format.
     *
     * @param listFile input CSV file
     * @param mapKeyNames keys for which the values are read from each line of
     * the input file
     * @return list with key-value entries read from the input file
     * @throws IOException
     */
    public static List<Map<String, String>> transformCSVFileToListOfMaps(File listFile, List<String> mapKeyNames) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(listFile));
        List<Map<String, String>> rtv = new LinkedList<>();
        String line = br.readLine();
        while (line != null) {
            String[] lineSplit = line.split(";");
            Map<String, String> lineMap = new HashMap<>();
            for (int i = 0; i < mapKeyNames.size(); i++) {
                lineMap.put(mapKeyNames.get(i), lineSplit[i]);
            }
            rtv.add(lineMap);
            line = br.readLine();
        }
        br.close();
        return rtv;
    }

    /**
     * Reads the CSV-like text file and returns a single list for each file
     * column. The lists are associated with the names specified within the
     * listKeyNames parameter.
     *
     * @param listFile input CSV file
     * @param listKeyNames column names, i.e., keys of read lists
     * @return map the with named columns (lists)
     * @throws IOException
     */
    public static Map<String, List<String>> transformCSVFileToMapOfLists(File listFile, List<String> listKeyNames) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(listFile));
        List<List<String>> lists = new ArrayList<>(listKeyNames.size());
        for (int i = 0; i < listKeyNames.size(); i++) {
            lists.add(new LinkedList<>());
        }
        String line = br.readLine();
        while (line != null) {
            String[] lineSplit = line.split(";");
            for (int i = 0; i < listKeyNames.size(); i++) {
                lists.get(i).add(lineSplit[i]);
            }
            line = br.readLine();
        }
        br.close();
        Map<String, List<String>> rtv = new HashMap<>(listKeyNames.size());
        for (int i = 0; i < listKeyNames.size(); i++) {
            rtv.put(listKeyNames.get(i), lists.get(i));
        }
        return rtv;
    }

    /**
     * Filters the list of maps by extracting the entries satisfying the
     * condition of the same value on the specified attribute.
     *
     * @param listOfMaps input list of maps to be filtered
     * @param matchAttributeName attribute whose value is matched
     * @param matchAttributeValue value according to which the entries are
     * filtered on the specified attribute
     * @return filtered list of maps
     */
    public static List<Map<String, String>> filterListOfMaps(List<Map<String, String>> listOfMaps, String matchAttributeName, String matchAttributeValue) {
        List<Map<String, String>> rtv = new LinkedList<>();
        for (Map<String, String> listEntry : listOfMaps) {
            String value = listEntry.get(matchAttributeName);
            if (matchAttributeValue.equals(value)) {
                rtv.add(listEntry);
            }
        }
        return rtv;
    }

    /**
     * Returns the color associated with the given sequence. If the color is not
     * specified for the given sequence, it is randomly created and stored.
     *
     * @param sequenceId id of the sequence
     * @param storeColor indicates whether the sequence-color association should
     * be stored, or not
     * @return the color associated with the given sequence
     */
    protected Color getSequenceColor(String sequenceId, boolean storeColor) {
        Color sequenceColor = sequenceColorMap.get(sequenceId);
        if (sequenceColor == null) {
            Random generator = new Random();
            // Luminance correction
            int rColor, gColor, bColor;
            do {
                rColor = generator.nextInt(256);
                gColor = generator.nextInt(256);
                bColor = generator.nextInt(256);
            } while (Math.sqrt(0.241 * rColor * rColor + 0.691 * gColor * gColor + 0.068 * bColor * bColor) > 0.85 * 255);
            sequenceColor = new Color(rColor, gColor, bColor);
            if (storeColor) {
                sequenceColorMap.put(sequenceId, sequenceColor);
            }
        }
        return sequenceColor;
    }

    /**
     * Normalizes the pose coordinates to be centered to the canvas and aligned
     * to the ground.
     *
     * @param o pose to be centered and aligned
     * @param groundCoord y-coordinate of the ground
     * @return centered and aligned pose coordinates
     */
    protected float[][] normalize(ObjectMocapPose o, float groundCoord) {

        // joint coordinates
        float[][] jointCoordinates = o.getJointCoordinates();
        float[][] normalizedCoordinates = new float[jointCoordinates.length][3];

        // Shifts the x-coordinate and z-coordinate to the canvas center
        float rX = -jointCoordinates[0][0];
        float rZ = -jointCoordinates[0][2];

        // Centers the skeleton coordinates to the canvas
        for (int i = 0; i < jointCoordinates.length; i++) {
            normalizedCoordinates[i][0] = jointCoordinates[i][0] + rX;
            normalizedCoordinates[i][1] = jointCoordinates[i][1] + groundCoord;
            normalizedCoordinates[i][2] = jointCoordinates[i][2] + rZ;
        }
        return normalizedCoordinates;
    }

    /**
     * Generates the sequence canvas and writes its to the JSP writer.
     *
     * @param out JSP writer
     * @param o sequence to be visualized
     * @param dist ranked sequence distance (can be <code>null</code>)
     * @throws IOException if there was an error writing to the page output
     */
    protected abstract void generateCanvas(JspWriter out, SequenceMocap<ObjectMocapPose> o, Float dist) throws IOException;

    //************ Implemented interface PageComponent ************//
    @Override
    public boolean pageComponentWrite(PageContext pageContext, Map<String, Object> variables) throws JspException, IOException {
        JspWriter out = pageContext.getOut();
        Object obj = variables.get("result");
        if (obj == null) {
            out.write("The result object was not provided!");
            return false;
        }
        if (obj instanceof RankedAbstractObject) {
            RankedAbstractObject ro = (RankedAbstractObject) obj;
            generateCanvas(out, (SequenceMocap<ObjectMocapPose>) ro.getObject(), ro.getDistance());
        } else if (obj instanceof SequenceMocap) {
            generateCanvas(out, (SequenceMocap<ObjectMocapPose>) obj, null);
        } else {
            out.write("The result object was not recognized!");
            return false;
        }
        return true;
    }

    //************ Dataset parameters ************//
    public static class DatasetDefinition {

        //************ Attributes ************//
        // name of the dataset
        protected final String name;
        // skeleton model name (used in javascript)
        protected final String skeletonModelName;
        // skeleton scaling (5.0 for the HDM05 dataset, 0.9 for the CMU dataset)
        protected final float skeletonEnlargement;

        //************ Constructors ************//
        /**
         * Creates a new instance of {@link DatasetDefinition}.
         *
         * @param name name of the dataset
         * @param skeletonModelName skeleton model name (used in javascript)
         * @param skeletonEnlargement skeleton scaling (5.0 for the HDM05
         * dataset, 0.9 for the CMU dataset)
         */
        public DatasetDefinition(String name, String skeletonModelName, float skeletonEnlargement) {
            this.name = name;
            this.skeletonModelName = skeletonModelName;
            this.skeletonEnlargement = skeletonEnlargement;
        }
    }
}
